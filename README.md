# Week 6 Mini-Project Requirements
Add logging to a Rust Lambda function

Integrate AWS X-Ray tracing

Connect logs/traces to CloudWatch

## Overview
For this project, I continued to use my Lambda function created in week5 and added logging and tracing to it. The function will return the name from the database that satisfied the given age and gender.

## Steps
1. The implementation steps to create a lambda function and deploy it to the AWS console can be found [here](https://gitlab.com/zibingao1010/week5).
2. Go to `Configuration` and go to `Monitoring and Operation tools`. Edit the `Additional monitoring tools` section to turn on Active tracing for `AWS X-Ray` and Enhanced monitoring for `CloudWatch Lambda Insights`. 
3. Then you'll be able to see the `CloudWatch log group` under `Logging configuration`.
4. Under `Traces` in `CloudWatch`, you will find the traces details.

## Screenshots
* Testing that shows the Lambda function is returning the correct name
![image](Test1.png)
![image](Test2.png)

* Screenshot showing tracing is turned on
![image](Turned-on.png)

* CloudWatch and X-Ray
![image](CloudWatch.png)
![image](X-Ray.png)

* Traces
![image](Traces1.png)
![image](Traces2.png)

* Logging
![image](Logging.png)




